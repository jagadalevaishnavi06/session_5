// Function to delete a row
function deleteRow(button) {
    var row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }
  
  // Function to open the update modal
  function openModal(button) {
    var row = button.parentNode.parentNode;
    var cells = row.getElementsByTagName("td");
    var updateName = document.getElementById("updateName");
    var updateEmail = document.getElementById("updateEmail");
    var updateAge = document.getElementById("updateAge");
    var updateCity = document.getElementById("updateCity");
  
    updateName.value = cells[1].textContent;
    updateEmail.value = cells[2].textContent;
    updateAge.value = cells[3].textContent;
    updateCity.value = cells[4].textContent;
  
    document.getElementById("myModal").style.display = "block";
  }
  
  // Function to close the update modal
  function closeModal() {
    document.getElementById("myModal").style.display = "none";
  }
  
  // Function to save the updated changes
  function saveChanges() {
    var updateName = document.getElementById("updateName").value;
    var updateEmail = document.getElementById("updateEmail").value;
    var updateAge = document.getElementById("updateAge").value;
    var updateCity = document.getElementById("updateCity").value;
  
    // Perform the update logic here with the updated values
  
    closeModal();
  }
  
  // Function to perform search
  function searchTable() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("dataTable");
    tr = table.getElementsByTagName("tr");
  
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1]; // Search based on the second column (Name)
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }
  